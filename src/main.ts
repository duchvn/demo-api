import { NestFactory } from '@nestjs/core';
import { json, urlencoded } from 'body-parser';
import { AppModule } from './app.module';
import {DocumentBuilder, SwaggerModule} from'@nestjs/swagger'
async function bootstrap() {
  
  const app = await NestFactory.create(AppModule, {cors:true});
  app.setGlobalPrefix('api')
  app.use(json());
  app.use(urlencoded({ extended: true }));
  app.enableCors();
  
  const options = new DocumentBuilder()
        .setTitle('API')
        .setDescription('CAUTION: This document is for internal use only')
        .setVersion('1')
        .addBearerAuth()
        .build();
    const document = SwaggerModule.createDocument(app, options);
    SwaggerModule.setup('documentation', app, document);


  await app.listen(3000);
}
bootstrap();
