import { ApiProperty, ApiResponseProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString, IsNumber, Length } from 'class-validator';

export interface AuthUserDto {
  id: number;
}

export class TokenPayloadDto {
  @ApiResponseProperty({
    example:
      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiaWF0IjoxNjIxNTk0MDM4LCJleHAiOjE2MjE1OTc2Mzh9.GXBCcTjKGNXwDMrdIo0LCg2b3_Clk9OO8WqoBSwuXl8',
  })
  readonly token: string;

  @ApiResponseProperty({ example: 3600 })
  readonly expiresIn: number;

  constructor(token: string, expiresIn: number) {
    this.token = token;
    this.expiresIn = expiresIn;
  }
}

export class MessageDto {
  readonly message: string;

  constructor(message: string) {
    this.message = message;
  }
}

export class LoginDto {
  @ApiProperty({ example: 'ducsine', required: true })
  @IsNotEmpty()
  @Length(5, 10)
  @IsString()
  username: string;

  @ApiProperty({ example: 'duc123', required: true })
  @IsNotEmpty()
  @Length(5, 10)
  @IsString()
  password: string;
}

export class RegisterDto {
  @ApiProperty({ example: 'ducsine', required: true })
  @IsNotEmpty()
  @Length(5, 10)
  @IsString()
  username: string;

  @ApiProperty({ example: 'duc123', required: true })
  @IsNotEmpty()
  @Length(5, 10)
  @IsString()
  password: string;

  @ApiProperty({ example: 'duc hoang', required: true })
  @IsNotEmpty()
  @Length(5, 10)
  @IsString()
  fullName: string;

  @ApiProperty({ example: 23, required: true })
  @IsNotEmpty()
  @IsNumber()
  age: number;
}

export class UpdateUserDto {
  @ApiProperty({ example: 'duc hoang', required: true })
  @Length(5, 10)
  @IsString()
  fullName?: string;

  @ApiProperty({ example: 23, required: true })
  @IsNumber()
  age?: number;
}
