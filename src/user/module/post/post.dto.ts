import { ApiProperty, ApiResponseProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString, IsNumber, Length } from 'class-validator';

export class PostDto {
  @ApiProperty({ example: 'ducsine', required: true })
  @IsNotEmpty()
  message: string;
}

export class createUserAndPostDto {
  @ApiProperty({ example: 'ducsine', required: true })
  @IsNotEmpty()
  @Length(5, 10)
  @IsString()
  username: string;

  @ApiProperty({ example: 'duc123', required: true })
  @IsNotEmpty()
  @Length(5, 10)
  @IsString()
  password: string;

  @ApiProperty({ example: 'duc hoang', required: true })
  @IsNotEmpty()
  @Length(5, 10)
  @IsString()
  fullName: string;

  @ApiProperty({ example: 23, required: true })
  @IsNotEmpty()
  @IsNumber()
  age: number;

  @ApiProperty({ example: 'ducsine', required: true })
  @IsNotEmpty()
  message: string;
}
