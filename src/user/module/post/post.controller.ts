import {
  Body,
  Controller,
  CustomDecorator,
  Get,
  HttpCode,
  HttpStatus,
  ParseIntPipe,
  Post,
  Query,
  Req,
  SetMetadata,
} from '@nestjs/common';
import { PostService } from './post.service';
import { Request } from 'express';
import { ApiBearerAuth, ApiOperation } from '@nestjs/swagger';
import { PostDto, createUserAndPostDto } from './post.dto';

const Public = (): CustomDecorator<string> => SetMetadata('isPublic', true);

@Controller('post')
export class PostController {
  constructor(private readonly _service: PostService) {}

  @Public()
  @ApiOperation({ tags: ['POST'] })
  @Get('allPost')
  async getAllPost() {
    return this._service.getAllPost();
  }

  @ApiBearerAuth()
  @ApiOperation({ tags: ['POST'] })
  @Get('getPostByUser')
  async getPostByUser(@Query('id', ParseIntPipe) id: number) {
    return this._service.getAllPostByUser(id);
  }

  @ApiBearerAuth()
  @ApiOperation({ tags: ['POST'] })
  @Post('createPost')
  async createPost(@Req() { user }: Request, @Body() message: PostDto) {
    console.log(user['id']);
    return this._service.newPost(user['id'], message.message);
  }

  @Public()
  @ApiOperation({ tags: ['POST'] })
  @Post('test')
  async rp() {
    console.log('invoke');
    return this._service.report();
  }

  @Public()
  @ApiOperation({ tags: ['POST'] })
  @HttpCode(HttpStatus.OK)
  @Post('createPostByUser')
  async creteUserAndPost(@Body() body: createUserAndPostDto) {
    return this._service.createUserAndPost(body);
  }

  @Public()
  @ApiOperation({ tags: ['POST'] })
  @HttpCode(HttpStatus.OK)
  @Get('getPostHavePage')
  async getData(
    @Query('context') context: string,
    @Query('p', ParseIntPipe) p: number,
    @Query('s', ParseIntPipe) s: number,
  ) {
    return this._service.getPost(p, s, context);
  }
}
