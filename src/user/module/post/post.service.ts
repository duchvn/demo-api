import { hashSync, genSaltSync } from 'bcryptjs';
import { Injectable } from '@nestjs/common';
import { PrismaService } from 'src/prisma/prisma.service';
import { RegisterDto } from 'src/user/dto/user.dto';
import { PostDto, createUserAndPostDto } from './post.dto';
@Injectable()
export class PostService {
  constructor(private prisma: PrismaService) {}

  async getAllPost() {
    return this.prisma.comment.findMany();
  }

  async getAllPostByUser(userId: number) {
    return this.prisma.comment.findMany({ where: { userId } });
  }

  async newPost(userId: number, message: string) {
    return this.prisma.comment.create({ data: { message, userId } });
  }

  async updatePostById(id: number, message: PostDto) {
    return this.prisma.comment.update({ where: { id }, data: message });
  }

  async deletePostById(id: number) {
    return this.prisma.comment.delete({ where: { id } });
  }

  async deletePostByUser(userId: number) {
    return this.prisma.comment.deleteMany({
      where: { userId },
    });
  }

  async countPostByUser(userId: number) {}

  async report() {
    return this.prisma.user.findMany({
      select: {
        id: true,
      },
    });
  }

  async createUserAndPost(input: createUserAndPostDto) {
    const password = hashSync(input.password, genSaltSync(10));

    const rs = await this.prisma.user.findFirst({
      where: { username: input.username },
    });

    return rs
      ? this.prisma.comment.create({
          data: { message: input.message, userId: rs.id },
        })
      : this.prisma.user.create({
          data: {
            fullName: input.fullName,
            age: input.age,
            username: input.username,
            password: hashSync(input.password, genSaltSync(10)),
            comment: {
              create: {
                message: input.message,
              },
            },
          },
        });
  }

  async getPost(page: number, size: number, context: string) {
    const where = { message: { contains: context } };
    const [total, data] = await Promise.all([
      this.prisma.comment.count({ where }),
      this.prisma.comment.findMany({ where, skip: page, take: size }),
    ]);
    return { total, data };
  }
}
