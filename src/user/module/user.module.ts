import { Module } from '@nestjs/common';
import { JwtModule, JwtService } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { PrismaService } from 'src/prisma/prisma.service';
import { JwtRolesAuthGuard } from '../jwt/jwt-roles.guard';
import { JwtStrategy } from '../jwt/jwt.trategy';
import { UserController } from './user.controller';
import { UserService } from './user.service';
import { AuthModule } from './auth/auth.module';
import { AuthService } from './auth/auth.service';
import { PostModule } from './post/post.module';
import { PostService } from './post/post.service';

@Module({
  imports: [
    PassportModule.register({ defaultStrategy: 'jwt', session: false }),
    JwtModule.registerAsync({
        useFactory: () => {
            return {
                secret: process.env['SECRET_KEY'],
                signOptions: { expiresIn: Number(process.env['JWT_EXPIRATION_TIME'])}
            };
        }
    }),
    AuthModule,
    PostModule
],
  controllers: [UserController],
  providers: [UserService, PrismaService, JwtStrategy, {provide: 'APP_GUARD', useClass: JwtRolesAuthGuard}, AuthService, PostService]
})
export class UserModule {}
