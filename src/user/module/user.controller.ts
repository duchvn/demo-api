import { JwtService } from '@nestjs/jwt';
import { Request } from 'express';
import { ApiBearerAuth, ApiOkResponse, ApiOperation } from '@nestjs/swagger';
import {
  LoginDto,
  MessageDto,
  RegisterDto,
  TokenPayloadDto,
  UpdateUserDto,
} from '../dto/user.dto';
import { UserService } from './user.service';
import {
  Body,
  Header,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Logger,
  Post,
  UsePipes,
  ValidationPipe,
  Patch,
  Query,
  Delete,
  UnauthorizedException,
  CustomDecorator,
  SetMetadata,
  Req,
} from '@nestjs/common';

const Public = (): CustomDecorator<string> => SetMetadata('isPublic', true);

@Controller('user')
export class UserController {
  private readonly _logger: Logger = new Logger('UserApi');

  constructor(
    private readonly _service: UserService,
    private readonly _jwtService: JwtService,
  ) {}

  @Public()
  @ApiOperation({ tags: ['USER'] })
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({
    type: RegisterDto,
    description: 'get user successfully.',
  })
  @Get('getUser')
  async getAll() {
    this._logger.log('getUser invoked.');
    console.log();
    return this._service.getAllUser();
  }

  @Public()
  @ApiOperation({ tags: ['USER'] })
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({
    type: TokenPayloadDto,
    description: 'login successfully.',
  })
  @UsePipes(new ValidationPipe())
  @Post('login')
  async login(@Body() body: LoginDto) {
    const user = await this._service.logIn(body);
    if (user) {
      const token = this._jwtService.sign(user);
      return new TokenPayloadDto(
        token,
        Number(process.env['JWT_EXPIRATION_TIME']),
      );
    } else throw new UnauthorizedException('Login failed!');
  }

  @Public()
  @ApiOperation({ tags: ['USER'] })
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({
    type: RegisterDto,
    description: 'Register successfully.',
  })
  @UsePipes(new ValidationPipe())
  @Post('/register')
  async usRegister(@Body() body: RegisterDto) {
    this._logger.log('register invoked.');
    return this._service.usRegister(body);
  }

  @ApiBearerAuth()
  @ApiOperation({ tags: ['USER'] })
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({
    type: MessageDto,
    description: 'update user successfully.',
  })
  @UsePipes(new ValidationPipe())
  @Patch('updateUser')
  async updateUser(@Body() body: UpdateUserDto, @Req() { user }: Request) {
    this._logger.log('update invoked.');
    this._service.updateUser(Number(user['id']), body);
    return new MessageDto('Update successfully!');
  }

  //
  @Public()
  @ApiOperation({ tags: ['USER'] })
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({ description: 'delete user successfully.' })
  @Delete('delUserById')
  async delUser(@Query('id') id: number) {
    this._logger.log('delUser invoked.');
    let _id = Number(id);
    return this._service.delUser(_id);
  }
}
