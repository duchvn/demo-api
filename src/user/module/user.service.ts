import { Injectable } from '@nestjs/common';
import { PrismaService } from 'src/prisma/prisma.service';
import { RegisterDto, LoginDto, UpdateUserDto } from '../dto/user.dto';
import { compareSync, genSaltSync, hashSync } from 'bcryptjs';

@Injectable()
export class UserService {
  constructor(private prisma: PrismaService) {}

  async logIn(input: LoginDto) {
    const user = await this.prisma.user.findFirst({
      select: { id: true, password: true },
      where: { username: input.username },
    });
    return compareSync(input.password, user.password) ? user : null;
  }

  async usRegister(input: RegisterDto) {
    await this.prisma.user.create({
      data: {
        ...input,
        password: hashSync(input.password, genSaltSync(10)),
      },
    });
  }

  async getAllUser() {
    return await this.prisma.user.findMany();
  }

  async updateUser(id: number, input: UpdateUserDto) {
    const rs = await this.prisma.user.update({ where: { id }, data: input });
    return rs;
  }

  async delUser(id: number) {
    await this.prisma.user.delete({ where: { id } });
  }
}
