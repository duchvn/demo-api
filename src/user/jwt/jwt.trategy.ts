import { config } from 'dotenv'
import { Injectable, Logger } from '@nestjs/common'
import { PassportStrategy } from '@nestjs/passport'
import { ExtractJwt, Strategy } from 'passport-jwt'
import { AuthUserDto } from '../dto/user.dto'

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
    constructor() {
        super(
            {
                jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
                ignoreExpiration: false,
                secretOrKey: process.env['SECRET_KEY']
            }
        )
    }

    async validate(payload: AuthUserDto): Promise<AuthUserDto> {
        return payload;
    }
}