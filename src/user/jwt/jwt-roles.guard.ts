import { ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { AuthGuard } from '@nestjs/passport';
import { Request } from 'express';
import { AuthUserDto } from '../dto/user.dto';

@Injectable()
export class JwtRolesAuthGuard extends AuthGuard('jwt') {
  constructor(private reflector: Reflector) {
    super();
  }

  async canActivate(context: ExecutionContext): Promise<boolean> {
    // Request is using Public decorator
    const isPublic = this.reflector.get<boolean>(
      'isPublic',
      context.getHandler(),
    );
    if (isPublic) {
      return true;
    }

    if (await super.canActivate(context)) {
      const request: Request = context.switchToHttp().getRequest();
      const user = request.user as AuthUserDto;

      if (user) {
        return true;
      }
    }
    return false;
  }
}
